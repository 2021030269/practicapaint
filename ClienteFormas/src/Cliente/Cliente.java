
package Cliente;

import java.net.*;
import java.io.*;
import java.util.logging.*;

public class Cliente extends javax.swing.JFrame {
    
    private PrintStream salida = null;
    private Socket ClienteSocket = null; // Socket 
    private String IP = "26.250.39.142"; //Ip, domino, o Nombre de la maquina
    private final int PUERTO = 5432;
    public Cliente() {
        initComponents();
        try {
            ClienteSocket = new Socket(IP, PUERTO);
            salida = new PrintStream(ClienteSocket.getOutputStream());
        } catch (UnknownHostException e) {
            System.err.println(IP + " desconocido");
            System.exit(1);
        } catch (IOException e) {
            System.err.println("No se puede establecer conexión");
            System.exit(1);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jInternalFrame1 = new javax.swing.JInternalFrame();
        Figura = new javax.swing.ButtonGroup();
        Colores = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        rbtnCirculo = new javax.swing.JRadioButton();
        rbtnRectangulo = new javax.swing.JRadioButton();
        jLabel2 = new javax.swing.JLabel();
        rbtnAzul = new javax.swing.JRadioButton();
        rbtnVerde = new javax.swing.JRadioButton();
        rbtnRojo = new javax.swing.JRadioButton();
        btnEnviar = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        jInternalFrame1.setVisible(true);

        javax.swing.GroupLayout jInternalFrame1Layout = new javax.swing.GroupLayout(jInternalFrame1.getContentPane());
        jInternalFrame1.getContentPane().setLayout(jInternalFrame1Layout);
        jInternalFrame1Layout.setHorizontalGroup(
            jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jInternalFrame1Layout.setVerticalGroup(
            jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("--------------Figura--------------");

        Figura.add(rbtnCirculo);
        rbtnCirculo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        rbtnCirculo.setText("Circulo");

        Figura.add(rbtnRectangulo);
        rbtnRectangulo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        rbtnRectangulo.setText("Rectángulo");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("-------------Colores--------------");

        Colores.add(rbtnAzul);
        rbtnAzul.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        rbtnAzul.setText("Azul");

        Colores.add(rbtnVerde);
        rbtnVerde.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        rbtnVerde.setText("Verde");
        rbtnVerde.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnVerdeActionPerformed(evt);
            }
        });

        Colores.add(rbtnRojo);
        rbtnRojo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        rbtnRojo.setText("Rojo");

        btnEnviar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnEnviar.setText("Enviar Tarea");
        btnEnviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnviarActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton1.setText("Salir");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnEnviar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rbtnRectangulo)
                            .addComponent(rbtnCirculo)
                            .addComponent(jLabel1)
                            .addComponent(rbtnAzul)
                            .addComponent(jLabel2)
                            .addComponent(rbtnVerde)
                            .addComponent(rbtnRojo))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rbtnCirculo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rbtnRectangulo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rbtnAzul)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rbtnVerde)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rbtnRojo)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEnviar)
                    .addComponent(jButton1))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rbtnVerdeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnVerdeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbtnVerdeActionPerformed

    private void btnEnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnviarActionPerformed
        
        salida.println(selectFig());
        Figura.clearSelection();
        Colores.clearSelection();
    }//GEN-LAST:event_btnEnviarActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        salida.println("Salir");
        try {
            ClienteSocket.close();
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton1ActionPerformed
//##########################################    
    public String selectFig(){
        int fig;
        int col;
        
        fig = selectForm();
        col = selectColor();
        return String.valueOf(fig + col);
    }
//##########################################    
    public int selectForm(){
        if(rbtnCirculo.isSelected()){
            return 10;
        }
        else if (rbtnRectangulo.isSelected()){
            return 20;
        }
        return -1;
    }
//##########################################
    public int selectColor(){
        if(rbtnAzul.isSelected()){
            return 1;
        }
        else if(rbtnVerde.isSelected()){
            return 2;
        }
        else if(rbtnRojo.isSelected()){
            return 3;
        }
        return -1;
    }
//##########################################
 
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Cliente().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup Colores;
    private javax.swing.ButtonGroup Figura;
    private javax.swing.JButton btnEnviar;
    private javax.swing.JButton jButton1;
    private javax.swing.JInternalFrame jInternalFrame1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JRadioButton rbtnAzul;
    private javax.swing.JRadioButton rbtnCirculo;
    private javax.swing.JRadioButton rbtnRectangulo;
    private javax.swing.JRadioButton rbtnRojo;
    private javax.swing.JRadioButton rbtnVerde;
    // End of variables declaration//GEN-END:variables
}
