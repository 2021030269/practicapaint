/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Servidor;

import java.awt.*;
import javax.swing.*;
import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Servidor extends JFrame{
    private  String valor;
    private DataInputStream entrada;
    private final int PUERTO = 5432;
    private  ServerSocket serverSocket = null;
    Formas  fr  = new Formas();  
    private JPanel dibujo;
  
    int color;
    int forma;
    public Servidor(){
        setTitle("Paint");
        setSize(250, 250);
        setVisible(true);
        dibujo = new JPanel(){
            @Override
            public void paint(Graphics g){
                super.paint(g);
                switch (valor) {
                    case "11":
                        g.setColor(Color.blue);
                        g.fillRoundRect(20, 20, 150, 150, 200, 200);
                        break;
                    case "12":
                        g.setColor(Color.green);
                        g.fillRoundRect(20, 20, 150, 150, 200, 200);
                        break;
                    case "13":
                        g.setColor(Color.red);
                        g.fillRoundRect(20, 20, 150, 150, 200, 200);
                        break;
                    case "21":
                        g.setColor(Color.blue);
                        g.fillRoundRect(20, 20, 150, 100, 0, 0);
                        break;
                    case "22":
                        g.setColor(Color.green);
                        g.fillRoundRect(20, 20, 150, 100, 0, 0);
                        break;
                    case "23":
                        g.setColor(Color.red);
                        g.fillRoundRect(20, 20, 150, 100, 0, 0);
                        break;
                }
            }
        };
        add(dibujo);
        Thread hilo = new Thread(fr);
        hilo.start();
        

    }
    
     class Formas extends Thread {
        @Override
        public void run(){
            try {
                serverSocket = new ServerSocket(PUERTO);
            } catch (IOException e) {
                System.err.println("No se puede escuchar por el puerto:" + PUERTO);
                System.exit(1);
            }

            Socket clientSocket = null;  // inicia el puerto cliente
            try {
                clientSocket = serverSocket.accept();// se conecta los Socket
                entrada = new DataInputStream(new BufferedInputStream(clientSocket.getInputStream()));
            } catch (IOException e) {
                System.err.println("Conexión fallida...");
                System.exit(1);
            }
            while (true) {
                try {
                    valor = entrada.readLine();  
                } catch (IOException e) {
                    System.out.println("El Mensaje no pudo ser Entregado");
                }
                if(valor.equals("Salir")){
                    break;
                }
                else{
                   dibujo.repaint();
                }
            }
            try {
                clientSocket.close();
                entrada.close();
                
            } catch (IOException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }  
        }  
    }
    
    public static void main(String[] args) {
       new Servidor();
    }
}
